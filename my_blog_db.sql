/*
 Navicat Premium Data Transfer

 Source Server         : localhost3306
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : my_blog_db

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 06/03/2022 15:15:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for generator_test
-- ----------------------------
DROP TABLE IF EXISTS `generator_test`;
CREATE TABLE `generator_test`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `test` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '测试字段',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for jdbc_test
-- ----------------------------
DROP TABLE IF EXISTS `jdbc_test`;
CREATE TABLE `jdbc_test`  (
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jdbc_test
-- ----------------------------
INSERT INTO `jdbc_test` VALUES ('com.zaxxer.hikari.HikariDataSource', 'hikari数据源');
INSERT INTO `jdbc_test` VALUES ('org.apache.commons.dbcp2.BasicDataSource', 'dbcp2数据源');
INSERT INTO `jdbc_test` VALUES ('test', '测试类');
INSERT INTO `jdbc_test` VALUES ('类别2', '测试类2');

-- ----------------------------
-- Table structure for tb_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin_user`;
CREATE TABLE `tb_admin_user`  (
  `admin_user_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `login_user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员登陆名称',
  `login_password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员登陆密码',
  `nick_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员显示昵称',
  `locked` tinyint(0) NULL DEFAULT 0 COMMENT '是否锁定 0未锁定 1已锁定无法登陆',
  PRIMARY KEY (`admin_user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_admin_user
-- ----------------------------
INSERT INTO `tb_admin_user` VALUES (1, 'liuhaibin', '5C7D445EE3C42205C76180E910368266', '劉海滨', 0);

-- ----------------------------
-- Table structure for tb_blog
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog`;
CREATE TABLE `tb_blog`  (
  `blog_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '博客表主键id',
  `blog_title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客标题',
  `blog_sub_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客自定义路径url',
  `blog_cover_image` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客封面图',
  `blog_content` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客内容',
  `blog_category_id` int(0) NOT NULL COMMENT '博客分类id',
  `blog_category_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客分类(冗余字段)',
  `blog_tags` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客标签',
  `blog_status` tinyint(0) NOT NULL DEFAULT 0 COMMENT '0-草稿 1-发布',
  `blog_views` bigint(0) NOT NULL DEFAULT 0 COMMENT '阅读量',
  `enable_comment` tinyint(0) NOT NULL DEFAULT 0 COMMENT '0-允许评论 1-不允许评论',
  `is_deleted` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0=否 1=是',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`blog_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_blog
-- ----------------------------
INSERT INTO `tb_blog` VALUES (1, '自我介绍', '', 'http://localhost:8081/admin/dist/img/rand/23.jpg', '## About me\n\n我是LHB，一名Java开发者，技术一般，经历平平，但是也一直渴望进步，同时也努力活着，为了人生不留遗憾，也希望能够一直做着自己喜欢的事情，得闲时分享心得、分享一些浅薄的经验，等以后老得不能再老了，就说故事已经讲完了,不去奢求圆满。\n\n相信浏览这段话的你也知道，学习是一件极其枯燥而无聊的过程，甚至有时候显得很无助，我也想告诉你，成长就是这样一件残酷的事情，任何成功都不是一蹴而就，需要坚持、需要付出、需要你的毅力，短期可能看不到收获，因为破茧成蝶所耗费的时间不是一天。\n\n## Contact\n\n- 我的邮箱：1626590287@qq.com\n- 我的QQ：1626590287', 24, '日常随笔', '世界上有一个很可爱的人', 1, 27, 0, 0, '2017-03-12 00:31:15', '2018-11-12 00:31:15');
INSERT INTO `tb_blog` VALUES (5, 'SSM整合配置', '', 'http://localhost:8081/admin/dist/img/rand/25.jpg', '# SSM整合\n\n## 1.Pom.xml\n\n```xml\n<!-- 单元测试 -->\n<dependency>\n    <groupId>junit</groupId>\n    <artifactId>junit</artifactId>\n    <version>4.12</version>\n    <scope>test</scope>\n</dependency>\n<dependency>\n    <groupId>org.springframework</groupId>\n    <artifactId>spring-test</artifactId>\n    <version>5.1.10.RELEASE</version>\n    <scope>test</scope>\n</dependency>\n<!-- 单元测试 -->\n\n<!-- 日志类库 -->\n<dependency>\n    <groupId>org.apache.logging.log4j</groupId>\n    <artifactId>log4j-slf4j-impl</artifactId>\n    <version>2.12.1</version>\n</dependency>\n<dependency>\n    <groupId>org.apache.logging.log4j</groupId>\n    <artifactId>log4j-web</artifactId>\n    <version>2.12.1</version>\n</dependency>\n<!-- 日志类库 -->\n\n<!-- mysql jdbc 驱动 -->\n<dependency>\n    <groupId>mysql</groupId>\n    <artifactId>mysql-connector-java</artifactId>\n    <version>8.0.16</version>\n</dependency>\n<!-- mysql jdbc 驱动 -->\n<!-- 数据库连接池 -->\n<!-- 导入druid的jar包，用来在applicationContext.xml中配置数据库 -->\n<dependency>\n    <groupId>com.alibaba</groupId>\n    <artifactId>druid</artifactId>\n    <version>1.1.20</version>\n</dependency>\n<!-- 导入dbcp的jar包，用来在applicationContext.xml中配置数据库 -->\n<!--    <dependency>-->\n<!--      <groupId>commons-dbcp</groupId>-->\n<!--      <artifactId>commons-dbcp</artifactId>-->\n<!--      <version>1.4</version>-->\n<!--    </dependency>-->\n<!-- 导入Cp30的jar包，用来在applicationContext.xml中配置数据库 -->\n<!--    <dependency>-->\n<!--      <groupId>com.mchange</groupId>-->\n<!--      <artifactId>c3p0</artifactId>-->\n<!--      <version>0.9.5.2</version>-->\n<!--    </dependency>-->\n<!-- 数据库连接池 -->\n\n<!-- mybatis -->\n<dependency>\n    <groupId>org.mybatis</groupId>\n    <artifactId>mybatis</artifactId>\n    <version>3.5.2</version>\n</dependency>\n<!-- mybatis -->\n\n<!--spring -->\n<dependency>\n    <groupId>org.springframework</groupId>\n    <artifactId>spring-webmvc</artifactId>\n    <version>5.1.10.RELEASE</version>\n</dependency>\n<!--spring -->\n<!--spring 和 aspectj 框架整合的模块-->\n<dependency>\n    <groupId>org.springframework</groupId>\n    <artifactId>spring-aspects</artifactId>\n    <version>5.1.10.RELEASE</version>\n</dependency>\n<!--spring 和 aspectj 框架整合的模块-->\n<!--spring 支持jdbc编程模块-->\n<dependency>\n    <groupId>org.springframework</groupId>\n    <artifactId>spring-jdbc</artifactId>\n    <version>5.1.10.RELEASE</version>\n</dependency>\n<!-- spring end -->\n<!-- mybatis 和 spring 整合依赖包 -->\n<dependency>\n    <groupId>org.mybatis</groupId>\n    <artifactId>mybatis-spring</artifactId>\n    <version>2.0.2</version>\n</dependency>\n<!-- mybatis 和 spring 整合依赖包 -->\n\n<!-- java web begin -->\n<dependency>\n    <groupId>javax.servlet</groupId>\n    <artifactId>javax.servlet-api</artifactId>\n    <version>3.1.0</version>\n    <scope>provided</scope>\n</dependency>\n<dependency>\n    <groupId>javax.servlet</groupId>\n    <artifactId>jstl</artifactId>\n    <version>1.2</version>\n</dependency>\n<dependency>\n    <groupId>javax.servlet.jsp</groupId>\n    <artifactId>jsp-api</artifactId>\n    <version>2.2</version>\n    <scope>provided</scope>\n</dependency>\n<!-- java web end -->\n\n<!-- spring-json依赖 -->\n<dependency>\n    <groupId>com.fasterxml.jackson.core</groupId>\n    <artifactId>jackson-core</artifactId>\n    <version>2.9.8</version>\n    </dependency>\n<dependency>\n    <groupId>com.fasterxml.jackson.core</groupId>\n    <artifactId>jackson-databind</artifactId>\n    <version>2.9.8</version>\n</dependency>\n<dependency>\n    <groupId>com.fasterxml.jackson.core</groupId>\n    <artifactId>jackson-annotations</artifactId>\n    <version>2.9.8</version>\n</dependency>\n<!-- spring-json依赖 -->\n\n<!--文件上传-->\n<dependency>\n    <groupId>commons-fileupload</groupId>\n    <artifactId>commons-fileupload</artifactId>\n    <version>1.3.3</version>\n</dependency>\n<!--文件上传-->\n```\n\n## 2.Web.xml\n\n```xml\n<!--编码过滤器-->\n<filter>\n  <filter-name>CharacterEncodingFilter</filter-name>\n  <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>\n  <init-param>\n    <param-name>encoding</param-name>\n    <param-value>UTF-8</param-value>\n  </init-param>\n</filter>\n<filter-mapping>\n  <filter-name>CharacterEncodingFilter</filter-name>\n  <url-pattern>/*</url-pattern>\n</filter-mapping>\n<!--     END        -->\n\n\n<!-- 增加HiddenHttpMethodFilte过滤器：给普通浏览器增加 put|delete请求方式 -->\n<filter>\n  <filter-name>HiddenHttpMethodFilte</filter-name>\n  <filter-class>org.springframework.web.filter.HiddenHttpMethodFilter</filter-class>\n</filter>\n<filter-mapping>\n  <filter-name>HiddenHttpMethodFilte</filter-name>\n  <!-- 过滤所有：/*-->\n  <url-pattern>/*</url-pattern>\n</filter-mapping>\n<!--     END        -->\n\n\n<!-- Spring配置文件信息 -->\n<context-param>\n  <param-name>contextConfigLocation</param-name>\n  <param-value>classpath:Spring/applicationContext-mybaits.xml</param-value>\n</context-param>\n<listener><!-- ContextLoaderListener监听器 -->\n  <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>\n</listener>\n<!--     END        -->\n\n\n<!--配置SpringMVC-->\n<servlet>\n  <servlet-name>SpringMVC</servlet-name>\n  <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>\n  <init-param>\n    <param-name>contextConfigLocation</param-name>\n    <!--加载spring主配置文件-->\n    <param-value>classpath:Spring/applicationContext-springmvc.xml</param-value>\n  </init-param>\n  <!--项目一启动就加载框架-->\n  <load-on-startup>1</load-on-startup>\n</servlet>\n<servlet-mapping>\n  <servlet-name>SpringMVC</servlet-name>\n  <url-pattern>/</url-pattern>\n</servlet-mapping>\n<!--     END        -->\n\n<!-- 404错误页面 -->\n<error-page>\n    <error-code>404</error-code>\n    <location>/404.jsp</location>\n</error-page>\n<!-- 404错误页面 -->\n\n<welcome-file-list>\n  <welcome-file>index.html</welcome-file>\n  <welcome-file>index.htm</welcome-file>\n  <welcome-file>index.jsp</welcome-file>\n</welcome-file-list>3.applicationContext-mybaits.xml\n```\n\n## 3.applicationContext-mybaits.xml\n\n```xml\n<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<beans xmlns=\"http://www.springframework.org/schema/beans\"\n	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:context=\"http://www.springframework.org/schema/context\"\n	xmlns:tx=\"http://www.springframework.org/schema/tx\"\n	xsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.2.xsd\n  	 http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.2.xsd\n 	 http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-4.2.xsd\">\n\n	<!-- 加载配数据源配置文件 db.properties -->\n	<context:property-placeholder location=\"classpath:config/db.properties\" />\n\n	<!-- 配置 C3P0 数据源 -->\n	<bean id=\"dataSource\" class=\"com.mchange.v2.c3p0.ComboPooledDataSource\" destroy-method=\"close\">\n		<property name=\"driverClass\" value=\"${datasource.connection.driver_class}\"/>\n		<property name=\"jdbcUrl\" value=\"${datasource.connection.url}\"/>\n		<property name=\"user\" value=\"${datasource.connection.username}\"/>\n		<property name=\"password\" value=\"${datasource.connection.password}\"/>\n	</bean>\n\n	<!-- 事务管理器 （JDBC） -->\n	<bean id=\"transactionManager\"\n		class=\"org.springframework.jdbc.datasource.DataSourceTransactionManager\">\n		<property name=\"dataSource\" ref=\"dataSource\"></property>\n	</bean>\n	<!-- 启动声明式事务驱动 -->\n	<tx:annotation-driven transaction-manager=\"transactionManager\" />\n\n\n	<!-- spring 通过 sqlSessionFactoryBean 获取 sqlSessionFactory 工厂类 -->\n	<bean id=\"sqlSessionFactory\" class=\"org.mybatis.spring.SqlSessionFactoryBean\">\n		<property name=\"dataSource\" ref=\"dataSource\"></property>\n		<!-- 扫描 po 包，使用别名 -->\n		<property name=\"typeAliasesPackage\" value=\"需要生成别名的包\"></property>\n		<!-- 扫描映射文件 -->\n		<property name=\"mapperLocations\" value=\"classpath:config/mybatis/mapper/*.xml\"></property>\n	</bean>\n\n	<!-- 配置扫描 dao 包，动态实现 dao 接口，注入到 spring 容器中 -->\n	<bean class=\"org.mybatis.spring.mapper.MapperScannerConfigurer\">\n		<property name=\"basePackage\" value=\"ＳＱＬ语句实现的包\" />\n		<!-- 注意使用 sqlSessionFactoryBeanName 避免出现spring 扫描组件失效问题 -->\n		<property name=\"sqlSessionFactoryBeanName\" value=\"sqlSessionFactory\" />\n	</bean>\n \n</beans>\n```\n\n## 4.mapper.xml\n\n```xml\n<!DOCTYPE mapper  \nPUBLIC \"-//mybatis.org//DTD Mapper 3.0//EN\"  \n\"http://mybatis.org/dtd/mybatis-3-mapper.dtd\">\n<mapper namespace=\"SQL语句实现的接口全类名\">\n</mapper>\n```\n\n## 5.db.properties\n\n```java\ndatasource.connection.driver_class=com.mysql.jdbc.Driver\ndatasource.connection.url=jdbc:mysql://127.0.0.1:3306/库名称?useUnicode=true&characterEncoding=utf-8\ndatasource.connection.username=root\ndatasource.connection.password=root\n```\n\n## 6.applicationContext-springmvc.xml\n\n```xml\n<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<beans xmlns=\"http://www.springframework.org/schema/beans\"\n	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:context=\"http://www.springframework.org/schema/context\"\n	xmlns:mvc=\"http://www.springframework.org/schema/mvc\" xmlns:aop=\"http://www.springframework.org/schema/aop\"\n	xmlns:task=\"http://www.springframework.org/schema/task\"\n	xsi:schemaLocation=\"http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc-4.2.xsd\n		http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.2.xsd\n		http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.2.xsd\n		http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-4.2.xsd\n	 	http://www.springframework.org/schema/task http://www.springframework.org/schema/task/spring-task-3.2.xsd\">\n\n	<!-- 只需要扫描包中的 Controller 注解 -->\n	<context:component-scan base-package=\"需要扫描的包名\"></context:component-scan>\n\n	<!-- 启动 mvc 注解驱动 -->\n	<mvc:annotation-driven></mvc:annotation-driven>\n	\n	<!-- 启动定时任务 -->\n	<task:annotation-driven/>\n	\n	<!-- 静态资源处理 -->\n	<mvc:default-servlet-handler/>\n	\n	<!-- 配置视图解析器 -->\n	<bean class=\"org.springframework.web.servlet.view.InternalResourceViewResolver\">\n		<property name=\"prefix\" value=\"/WEB-INF/views/\"></property>//前缀\n		<property name=\"suffix\" value=\".jsp\"></property>后缀\n	</bean>\n	\n	<!-- 文件上传 -->\n       <bean id=\"multipartResolver\" class=\"org.springframework.web.multipart.commons.CommonsMultipartResolver\">\n		        <!-- 请求的编码格式, 和 jsp 页面一致 -->\n			<property name=\"defaultEncoding\" value=\"UTF-8\"></property>\n			<!-- 上传单个文件的最大值，单位Byte;如果-1，表示无限制    上传文件大小限制 -->\n			<property name=\"maxUploadSize\"  value=\"10485760\"></property>\n	    </bean>\n	\n	<!-- 后台访问拦截器 -->\n	 <mvc:interceptors>\n		<mvc:interceptor>\n			<mvc:mapping path=\"/**\"/>\n			<mvc:exclude-mapping path=\"/system/login\"/>\n			<mvc:exclude-mapping path=\"/system/get_cpacha\"/>\n			<mvc:exclude-mapping path=\"/h-ui/**\"/>\n			<mvc:exclude-mapping path=\"/easyui/**\"/>\n			<bean class=\"拦截器全类名\"></bean>\n		</mvc:interceptor>\n	</mvc:interceptors> \n</beans>\n```\n\n## 7.log4j2.xml\n\n```xml\n<configuration>\n<!--先定义所有的appender-->\n    <appenders>\n        <!--输出控制台的配置-->\n        <console name=\"Console\" target=\"SYSTEM_OUT\">\n            <!--输出日志的格式-->\n            <!--<patternlayout pattern=\"%d{yyyy-MM-dd HH:mm:ss} [%p] %c %m %n\"/>-->\n            <patternlayout pattern=\"[%p] %m %n\"/>\n        </console>\n    </appenders>\n<!-- 然后定义logger，只有定义了logger并引入的appender，appender才会生效-->\n<!-- 日志级别以及优先级排序: OFF > FATAL > ERROR > WARN > INFO > DEBUG > TRACE > ALL -->\n    <loggers>\n        <root level=\"DEBUG\">\n        <!--输出到控制台-->\n            <appender-ref ref=\"Console\"/>\n        </root>\n<!--org.springframework<logger name=\"org.springframework\" level=\"INFO\"/>-->\n    </loggers>\n</configuration>\n```', 22, 'SSM', '配置文件,Spring,SpringMVC,MyBaits', 1, 15, 1, 0, '2020-11-03 17:17:37', '2020-11-03 17:17:37');
INSERT INTO `tb_blog` VALUES (6, 'SpringBoot实战_MyBatis', '', 'http://localhost:8081/admin/dist/img/rand/29.jpg', '# springboot整合mybatis\n\n## 1.导入依赖\n\n```xml\n<dependency>\n      <groupId>org.mybatis.spring.boot</groupId>\n      <artifactId>mybatis-spring-boot-starter</artifactId>\n      <version>2.1.3</version>\n</dependency>\n<dependency>\n       <groupId>org.springframework.boot</groupId>\n       <artifactId>spring-boot-starter-jdbc</artifactId>\n</dependency>\n<dependency>\n       <groupId>mysql</groupId>\n       <artifactId>mysql-connector-java</artifactId>\n       <scope>runtime</scope>\n</dependency>\n```\n\n也可以在创建 boot项目 时候 勾选  Mybatis组件\n\n## 2.配置数据库连接信息\n\n```yaml\nspring:\n datasource:\n   username: root\n   password: root\n   url: jdbc:mysql://localhost:3306/db_2018131644?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8\n   driver-class-name: com.mysql.cj.jdbc.Driver\n\n\nmybatis:\n  mapper-locations: classpath:mapper/*.xml    #需要修改\n  type-aliases-package: com.shirospbt.demo.pojo #需要修改\n\nserver:\n  port: 8888 #端口号\n```\n\n## 3.创建实体类，mapper映射文件，和mapper接口文件\n\n```xml\n<!-- mapper映射文件-->\n<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<!DOCTYPE mapper\n        PUBLIC \"-//mybatis.org//DTD Mapper 3.0//EN\"\n        \"http://mybatis.org/dtd/mybatis-3-mapper.dtd\">\n<mapper namespace=\"com.liu.db.Usermapper\">\n    <select id=\"getDepartments\" resultType=\"Department\">\n       select * from department;\n    </select>\n</mapper>\n```\n\nmapper映射文件应该放在 resources 目录下 并且要放在跟数据库配置里面的  mapper-locations的相同目录下\n\n``` java\n//实体类\nimport lombok.AllArgsConstructor;\nimport lombok.Data;\nimport lombok.NoArgsConstructor;\n\n@Data\n@NoArgsConstructor\n@AllArgsConstructor\npublic class Department {\n\n    private Integer id;\n    private String departmentName;\n\n}\n```\n\n``` java \n//mapper接口类\n@Mapper\n@Repository\npublic interface DepartmentrMapper {\n   User getDepartments();\n}\n```\n\n## 4.编写controller进行测试\n\n``` java \n@RestController\npublic class DepartmentController {\n    \n    @Autowired\n    DepartmentMapper departmentMapper;\n    \n    // 查询全部部门\n    @GetMapping(\"/getDepartments\")\n    public List<Department> getDepartments(){\n        return departmentMapper.getDepartments();\n    }\n}\n```\n\n启动项目  ，地址栏输入请求，看到返回的josn对象  就是测试成功', 25, 'SpringBoot', '配置文件,SpringBoot,Mybaits', 1, 28, 1, 0, '2020-11-04 09:49:00', '2020-11-04 09:49:00');
INSERT INTO `tb_blog` VALUES (7, 'SpringBoot基础知识点', '', '/admin/dist/img/rand/40.jpg', '# SpringBoot基础知识点\n\n## 1.Spring Boot 简介\n\n+ 简化Spring应用开发的一个框架；\n\n+ 整个Spring技术栈的一个大整合；\n\n+ J2EE开发的一站式解决方案；\n\n## 2.springboot 项目搭建\n\n前提：maven +  jdk8  安装好\n\nIDEA：通过  脚手架  进行项目生成\n\nSTS： 通过 new  Flie   进行springboot项目创建\n\n## 3.运行原理\n\n我们之前写的HelloSpringBoot，到底是怎么运行的呢，Maven项目，我们一般从pom.xml文件探究起；\n\n> 父依赖\n\n其中它主要是依赖一个父项目，主要是管理项目的资源过滤及插件！\n\n``` xml\n<parent>    \n    <groupId>org.springframework.boot</groupId>\n    <artifactId>spring-boot-starter-parent</artifactId>\n    <version>2.2.5.RELEASE</version>        \n    <relativePath/> <!-- lookup parent from repository -->\n</parent>\n```\n\n点进去，发现还有一个 父依赖\n\n``` xml \n<parent>         \n    <groupId>org.springframework.boot</groupId>         \n    <artifactId>spring-boot-dependencies</artifactId>       \n    <version>2.2.5.RELEASE</version>         \n    <relativePath>../../spring-boot-dependencies</relativePath>\n</parent>\n```\n\n这里才是真正管理SpringBoot应用里面所有依赖版本的地方，SpringBoot的版本控制中心；\n\n**以后我们导入依赖默认是不需要写版本；但是如果导入的包没有在依赖中管理着就需要手动配置版本了；**\n\n启动器 spring-boot-starter\n\n``` xml\n<dependency>        \n    <groupId>org.springframework.boot</groupId>        \n    <artifactId>spring-boot-starter-web</artifactId>\n</dependency>\n```\n\n**springboot-boot-starter-xxx**：就是spring-boot的场景启动器\n\n**spring-boot-starter-web**：帮我们导入了web模块正常运行所依赖的组件；\n\nSpringBoot将所有的功能场景都抽取出来，做成一个个的starter （启动器），只需要在项目中引入这些starter即可，所有相关的依赖都会导入进来 ， 我们要用什么功能就导入什么样的场景启动器即可 ；我们未来也可以自己自定义 starter；\n\n主启动类\n\n## 4.配置文件\n\n> resources文件：\n\n​	static:静态资源（js css 图片 音频 视频）\n\n​	templates：模板文件（模版引擎freemarker ,thymeleaf；默认不支持jsp）\n\n​	application.properties： 配置文件\n\n配置文件的作用：修改SpringBoot自动配置的默认值；SpringBoot在底层都给我们自动配置好；\n\nSpringBoot使用一个全局的配置文件，配置文件名是固定的；\n\n> application.properties    \n\n​        语法结构 ：key=value          或行内写法(    k： v     [Set/List/数组]         {map,对象类型的属性}，      并且 [ ]可省{ }不能省)\n\n> application.yml \n\n​         语法结构 ：key= 空格 value     属性和值也是大小写敏感；\n\n​         注意：1. k:空格v     2.通过垂直对齐 指定层次关系	  3.默认可以不写引号；  \"\"会将其中的转义符进行转义，其他不会\n\nserver:  port: 8088\n\nSpring boot官方推荐使用  yml  进行配置  所以  主要 介绍yml  的赋值 \n\n### 1 普通值\n\n**字面量：普通的值（数字，字符串，布尔）**\n\n+ k: 空格 v：字面直接来写；  字符串默认不用加上单引号或者双引号；\n\n​          \"\"：双引号；不会转义字符串里面的特殊字符；特殊字符会作为本身想表示的意思\n\n​         name: \"zhangsan \\n lisi\"：输出；zhangsan 换行 lisi\n\n​         \'\'：单引号；会转义特殊字符，特殊字符最终只是一个普通的字符串数据\n\n​         name: ‘zhangsan \\n lisi’：输出；zhangsan \\n lisi\n\n### 2 map 类型  对象类型\n\n+ 对象、Map（属性和值）（键值对）：\n\n+ k: v：在下一行来写对象的属性和值的关系；注意缩进\n\n+ 对象还是k: v的方式   friends:   lastName: zhangsan   age: 20 \n\n  ​                  行内写法：friends: {lastName: zhangsan,age: 18}\n\n### 3 数组（List、Set\n\n+ 数组（List、Set）：用- 值表示数组中的一个元素\n\npets:    ‐ cat    ‐ dog    ‐ pig 行内写法  pets: [cat,dog,pig]  \n\n### 4.属性注入\n\n> 通过@value\n\n```java\n @Component //注册bean \npublic class Dog {    \n    @Value(\"阿黄\")        \n    private String name;        \n    @Value(\"18\")        \n    private Integer age; \n}\n```\n> 通过 @ConfigurationProperties (prefix = \"person\")\n\n``` yml\nperson: \n   lastName: hello \n   age: 18 \n   boss: false \n   birth: 2017/12/12 \n   maps: {k1: v1,k2: 12} \n   lists: ‐ lisi ‐ zhaoliu \n   dog: name: 小狗 \n   age: 12\n```\n\n``` java\n// 将配置文件中配置的每一个属性的值，映射到这个组件中 //\n//@ConfigurationProperties：告诉SpringBoot将本类中的所有属性和配置文件中相关的配置进行绑定；通过 prefix = \"person\"：配置文件中哪个下面的所有属性进行一一映射    只有这个组件是容器中的组件，才能容器提供的@ConfigurationProperties功能； // \n@Component \n@ConfigurationProperties(prefix = \"person\") \npublic class Person { \n    private String lastName; \n    private Integer age; \n    private Boolean boss; \n    private Date birth; \n    private Map<String,Object> maps;\n    private List<Object> lists; \n    private Dog dog;\n```\n\n使用这个注解时候IDEA  会提示找不到 导入配置 即可\n\n``` xml\n<!‐‐导入配置文件处理器，配置文件进行绑定就会有提示‐‐> \n<dependency>\n    <groupId>org.springframework.boot</groupId>\n    <artifactId>spring‐boot‐configuration‐processor</artifactId>\n    <optional>true</optional> \n</dependency>\n```\n\n> 通过指定配置文件 注入\n\n**@PropertySource**：加载指定的配置文件；  但是，@PropertySource只能加载 properties，不能加载 yml\n\n**@configurationProperties**：默认从全局配置文件中获取值；\n\n``` java\n@PropertySource(value = \"classpath: person.properties\") \n@Component //注册bean \npublic class Person {    \n     @Value(\"${name}\")        \n     private String name;   \n      ......   \n}\n```\n\n> 回顾 使用 properties 注入\n\n【注意】properties配置文件在写中文的时候，会有乱码 ， 我们需要去IDEA中设置编码格式为UTF-8；\n\nsettings-->FileEncodings 中配置\n\n``` properties\nuser1.name=kuangshen \nuser1.age=18 \nuser1.sex=男\n```\n\n``` java \n@Component //注册bean \n@PropertySource(value = \"classpath:user.properties\")//也可以使用@configurationProperties  一次性注入 \npublic class User {    \n    //直接使用@value        \n    @Value(\"${user.name}\") \n    //从配置文件中取值        \n    private String name;        \n    @Value(\"#{9*2}\")  \n    // #{SPEL} Spring表达式        \n    private int age;       \n    @Value(\"男\")  \n    // 字面量        \n    private String sex; \n}\n```\n\n@Value这个使用起来并不友好！我们需要为每个属性单独注解赋值，比较麻烦；我们来看个功能对比图\n\n![img](https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=743784813,2689316743&fm=26&gp=0.jpg)\n\n1. @ConfigurationProperties只需要写一次即可 ， @Value则需要每个字段都添加\n\n2. 松散绑定：这个什么意思呢? 比如我的yml中写的last-name，这个和lastName是一样的， - 后面跟着的字母默认是大写的。这就是松散绑定。可以测试一下\n\n3. JSR303数据校验 ， 这个就是我们可以在字段是增加一层过滤器验证 ， 可以保证数据的合法性\n\n4. 复杂类型封装，yml中可以封装对象 ， 使用value就不支持\n\n**结论：**\n\n+ 配置yml和配置properties都可以获取到值 ， 强烈推荐 yml；\n\n+ 如果我们在某个业务中，只需要获取配置文件中的某个值，可以使用一下 @value；\n\n+ 如果说，我们专门编写了一个JavaBean来和配置文件进行一一映射，就直接@configurationProperties，不要犹豫！\n\n  \n\n## 5.spring boot全局配置文件中的 占位符表达式\n\n1. 随机数 ${random.uuid}等\n\n2. 引用变量值 \n\n``` yaml\nstudent:\n     name:  ${student.user.name}\n```\n\n实际引用的是properties中的student.user.name=zl67\n\n``` yaml\n${random.value}、${random.int}、${random.long} ${random.int(10)}、${random.int[1024,65536]}\n```\n\n\n\n## 6.多环境的切换（profile)\n\n> properties文件\n\n​	默认boot会读取application.properties环境   端口号8882\n\n​	多个：\n\n​	application-环境名.properties\n\n​	application-dev.properties   端口号8883\n\n​	application-test.properties   端口号8884\n\n如果要选择某一个具体的环境： application.properties中指定：spring.profiles.active=环境名\n\n如果将application.properties注释掉，spring boot仍然会读取其他appilcation-环境名.properties中的配置。并且properties的优先级高于yml\n\n> yml  文件\n\n``` yaml\n	第一个环境（主环境） \n	server:  		\n	    port: 8883 \n    spring: 	 	\n	     profiles:   			 \n	          active: dev  指定本次采用的环境  （这样子启动后端口号因为指定了是dev 所以端口号为8884 ） \n	第二个环境 --- \n	server: 	\n	    port: 8884 \n	spring: 		\n	    profiles: dev  环境名\n```\n\n\n\n> 动态切换环境\n\n​	 	i:通过运行参数指定环境\n\n​		（1）STS(Eclipse) ：Run Configuration - Argument - program Argument	\n\n​			--spring.profiles.active=环境名\n\n​		   (2)命令行方式：\n\n​			java -jar 项目名.jar --spring.profiles.active=环境名\n\n​		ii:通过虚拟机参数指定环境\n\n​			STS(Eclipse) ：Run Configuration - Argument - VM	\n\n​			-Dspring.profiles.active=环境名\n\n## 7.配置文件加载顺序\n\n> 1.项目内部的配置文件：\n\n​	properties和yml中的配置，相互补充；如果冲突，则properties优先级高。\n\n​	spring boot默认能够读取的application.properties/application.yml，这2个文件 可以存在于以下4个地方：\n\n​	file:项目根目录/config	 	application.properties\n\n​	file:项目根目录			application.properties\n\n​	classpath:项目根目录/config	application.properties\n\n​	classpath:项目根目录		application.properties	\n\n注意:			\n\n**如果某项配置冲突，则优先级从上往下**\n**如果不冲突，则互补结合使用**\n\n配置项目名：   properties文件中       server.servlet.context-path=/boot\n\n**2.项目外部的配置文件： (补救)**\n\n​	在项目Run ->configuration ->argumenets: \n\n```xml\n--spring.config.location=D:/application.properties\n```\n\n​	如果 同一个配置 同时存在于 内部配置文件 和外部配置文件，则外部>内部\n\n​	HW.jar   运行，8881--8882\n\n​	外部配置文件\n\n​	通过命令行 调用外部配置文件\n\n​	java -jar  项目.jar  \n\n```xml \n--spring.config.location=D:/application.properties\n```\n\n**3.项目运行参数： (补救)**\n\n​	在项目Run configuration ,argumenets:\n\n​	--server.port=8883\n\n​	通过命令行 调用外部配置文件\n\n​	java -jar  项目.jar  --server.port=8883\n\n​	多个地方配置时，如果冲突，优先级：\n\n​	命令参数（调用外部的配置文件 > 运行参数 ）>内部文件 (properties>yaml)\n\n[官网对多配置时的顺序说明](https://docs.spring.io/spring-boot/docs/2.0.4.RELEASE/reference/htmlsingle/#boot-features-external-config)\n\n## 9.日志\n\n日志框架 UCL    JUL     jboss-logging     logback     log4j    log4j2      slf4j    ...\n	spring boot默认选用slf4j，logback\n	spring boot默认帮我们配置好了日志，我们直接使用即可\n\n**日志级别：**\n		TRACE< DEBUG< INFO<WARN< ERROR< FATAL<OFF\n\n``` java \n   void contextLoads() {\n//日志级别：\n//		TRACE< DEBUG< INFO<WARN< ERROR< FATAL<OFF\n        logger.trace(\"这是trace+++++++++++++++\");\n        logger.debug(\"这是deBug+++++++++++++++\");\n        //springboot默认的日志级别是info（即只打印  info及之后级别的信息）\n        logger.info(\"这是info+++++++++++++++\");\n        logger.warn(\"这是warn+++++++++++++++\");\n        logger.error(\"这是error+++++++++++++++\");\n   }\n```\n\nspringboot默认的日志级别是info（即只打印  info及之后级别的信息）\n\n可以通过修改配置文件改变日志级别\n\n> yml\n\n```  yml\nlogging:\n      level:\n         com.liuhaib: trace    //指定包下   的级别\n```\n\n> properties\n\n``` properties\nlogging.level.com.liuhaib=trace   //指定包下   的级别\n```\n**logging.file  和  logging.path**\n\n可以通过配置 将日志信息 存储到文件中   logging.file=springboot.log    存储到了项目的根目录中的springboot.log\n也可以指定 具体的日志路径：logging.file=D:/springboot.log\n也可以存储到一个 文件夹中 ，=D:/log/，并且默认的文件名是spring.log\n\n| logging.file | logging.path | Example  | Description                        |\n| ------------ | ------------ | -------- | ---------------------------------- |\n| (none)       | (none)       | (none)   | 只在控制台输出                     |\n| 指定文件名   | (none)       | my.log   | 输出日志到my.log文件               |\n| (none)       | 指定目录     | /var/log | 输出到指定目录的 spring.log 文件中 |\n\n**指定日志显示格式：**\n\n通过配置文件我们还可以修改日志输出的格式：\n\n​		a.日志显示在console中\n\n``` properties\nlogging.pattern.console=%d{yyyy-MM-dd} [%thread] %-5level %logger{50} - %msg%n\n					%d:日期时间\n					%thread：线程名\n					%-5level： 显示日志级别,-5表示从左显示5个字符宽度\n					%logger{50} :设置日志长度  ，例如o.s.w.s.m.m.a.\n                    %msg：日志消息\n					%n ：回车\n```\n\n​		b.日志显示在文件中\n\n``` properties\nlogging.pattern.file=%d{yyyy-MM-dd} ** [%thread] ** %-5level ** %logger{50}** %msg%n\n```\n\n**通过xml日志格式设置：**\n	日志的具体使用规范：[官方说明](https://docs.spring.io/spring-boot/docs/2.0.4.RELEASE/reference/htmlsingle/#boot-features-custom-log-configuration)\n\n根据日志记录系统的不同，将加载以下文件：\n\n| 测井系统               | 定制化                                                       |\n| ---------------------- | ------------------------------------------------------------ |\n| Logback                | `logback-spring.xml`, `logback-spring.groovy`, `logback.xml`，或`logback.groovy` |\n| Log4j2                 | `log4j2-spring.xml`或`log4j2.xml`                            |\n| JDK(Java Util日志记录) | `logging.properties`                                         |\n\n官方文档推荐 使用`-spring`日志配置的变体(例如，`logback-spring.xml`而不是`logback.xml`)。如果使用标准配置位置，Spring无法完全控制日志初始化\n\n所以日常使用建议：logback-spring.xml  命名\n\n**logback.xml**：直接就被日志框架识别了；\n**logback-spring.xml**：日志框架就不直接加载日志的配置项，由SpringBoot解析日志配置，可以使用SpringBoot  的高级Profile功能\n\n通过logback-spring.xml文件我们可以\n\n```xml\n<springProfile name=\"staging\">\n<!‐‐ configuration to be enabled when the \"staging\" profile is active ‐‐>\n可以指定某段配置只在某个环境下生效\n</springProfile>\n```\n\n如：\n\n```xml\n<appender name=\"stdout\" class=\"ch.qos.logback.core.ConsoleAppender\">\n<!‐‐\n日志输出格式：\n%d表示日期时间，\n%thread表示线程名，\n%‐5level：级别从左显示5个字符宽度\n%logger{50} 表示logger名字最长50个字符，否则按照句点分割。\n%msg：日志消息，\n%n是换行符\n‐‐>\n<layout class=\"ch.qos.logback.classic.PatternLayout\">\n<springProfile name=\"dev\">\n<pattern>%d{yyyy‐MM‐dd HH:mm:ss.SSS} ‐‐‐‐> [%thread] ‐‐‐> %‐5level %logger{50} ‐ %msg%n</pattern>\n</springProfile>\n<springProfile name=\"!dev\">\n<pattern>%d{yyyy‐MM‐dd HH:mm:ss.SSS} ==== [%thread] ==== %‐5level %logger{50} ‐ %msg%n</pattern>\n</springProfile>\n</layout>\n</appender>\n```\n\n如果使用logback.xml作为日志配置文件，还要使用profile功能，会有以下错误\n`no applicable action for [springProfile]`\n\n**切换日志框架**\n\nslf4j+log4j的方式：\n\n```xml\n<dependency>\n    <groupId>org.springframework.boot</groupId>\n    <artifactId>spring‐boot‐starter‐web</artifactId>\n    \n    <exclusions><!--排除依赖-->\n       <exclusion>\n         <artifactId>logback‐classic</artifactId>\n         <groupId>ch.qos.logback</groupId>\n       </exclusion>\n       <exclusion>\n         <artifactId>log4j‐over‐slf4j</artifactId>\n         <groupId>org.slf4j</groupId>\n       </exclusion>\n   </exclusions>\n    \n</dependency>\n\n<dependency>\n    <groupId>org.slf4j</groupId>\n    <artifactId>slf4j‐log4j12</artifactId>\n</dependency>\n```\n\n引入log4j 的 配置文件 即可\n\nlog4j2 ：\n\n``` xml\n<dependency>\n    <groupId>org.springframework.boot</groupId>\n    <artifactId>spring‐boot‐starter‐web</artifactId>\n    \n    <exclusions><!--排除依赖-->\n        <exclusion>\n            <artifactId>spring‐boot‐starter‐logging</artifactId>\n            <groupId>org.springframework.boot</groupId>\n        </exclusion>\n    </exclusions>\n    \n</dependency>\n\n<dependency>\n    <groupId>org.springframework.boot</groupId>\n    <artifactId>spring‐boot‐starter‐log4j2</artifactId>\n</dependency>\n```\n\nlog4j2 在springboot 有具有默认配置文件', 25, 'SpringBoot', '学习笔记', 1, 58, 1, 0, '2020-11-04 10:07:42', '2020-11-04 10:07:42');
INSERT INTO `tb_blog` VALUES (9, 'MySQL访问量代码', '', 'http://localhost:8081/admin/dist/img/rand/7.jpg', '## MySQL查询——（一年内每个月的数据量）\n\n```sql\n     SELECT MONTH\n			( e.时间的字段 ) AS DaTe,\n			count( * ) AS DateCount \n		FROM\n			自己的表 e \n		WHERE\n			e.时间的字段 between  \"2020-01-01\"  and  \"2020-12-31\"\n		GROUP BY\n			MONTH ( e.时间的字段 ) \n		ORDER BY\n			MONTH ( e.时间的字段 );\n			\n		//只会显示有数据的查询结果\n```\n\n## MySQL查询——（每个月内的每天的数据量）\n\n``` sql\n SELECT DAY\n			( e.时间的字段 ) AS DaTe,\n			count( * ) AS DateCount \n		FROM\n			自己的表 e \n		WHERE\n			e.时间的字段 between  \"2020-01-01\"  and  \"2020-01-31\"\n		GROUP BY\n			DAY ( e.时间的字段 ) \n		ORDER BY\n			DAY ( e.时间的字段 );\n			\n	//同时适用于在一周内每天的数据查询\n```\n\n### MySQL查询——（今天内24小时每个小时的数据量）\n\n``` sql\nSELECT HOUR\n			( e.时间的字段 ) AS DaTe,\n			count( * ) AS DateCount \n		FROM\n			自己的表 e \n		WHERE\n			e.create_time between  \"2020-01-01 00:00:00\" and  \"2020-01-02 00:00:00\" \n		GROUP BY\n			HOUR ( e.时间的字段 ) \n		ORDER BY\n			HOUR ( e.时间的字段 );\n```', 26, 'MySQL', 'MySQL', 1, 2, 1, 0, '2021-12-04 14:12:43', '2021-12-04 14:12:43');
INSERT INTO `tb_blog` VALUES (10, 'markdown语法', '', 'http://localhost:8081/admin/dist/img/rand/2.jpg', '# 语法学习\n\n## 标题\n\n语法：\n\n``` xml\n#(空格)内容     回车  控制标题\n```\n\n### 三级标题\n\n## 字体\n\n加粗 ：**hello world**\n\n斜体   ：*hello world* \n\n斜体加粗   ：***hello world*** \n\n删除线   ：~~hello world~~ \n\n## 引用\n\n语法：\n\n``` xml\n< 内容\n```\n\n\n\n> 伟大的祖国\n\n## 分割线\n\n---三个横   或者  ***三个星\n\n***\n\n---\n\n## 图片\n\n语法：\n\n ``` xml\n![名字](文件的路径   /可以是本地的也可以网络地址)\n ```\n\n\n\n![](http://localhost:8081/upload/20211204_14152050.jpg)\n\n## 超链接\n\n语法：\n\n``` xaml\n[连接名称](网络地址)\n```\n\n[bilibili](https://www.bilibili.com)\n\n## 列表\n\n有序列表：\n\n``` xml\n1.空格\n```\n\n1. 一\n2. 二\n3. 三\n\n无序列表：\n\n``` xml\n+空格\n```\n\n+ yi\n+ ew\n+ san\n\n## 表格\n\n直接右键插入算了\n\n|      |      |      |\n| ---- | ---- | ---- |\n|      |      |      |\n|      |      |      |\n|      |      |      |\n\n## 代码\n\n语法：\n\n``` xml\n​``` 代码类型   回车\n```\n\n## 换行\n\n在需要换行的地方敲两个空格\n\n``` java \na.配置（空格空格）  \n  配置\n```', 27, 'markdown', 'markdown', 1, 1, 1, 0, '2021-12-04 14:15:48', '2021-12-04 14:15:48');

-- ----------------------------
-- Table structure for tb_blog_category
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_category`;
CREATE TABLE `tb_blog_category`  (
  `category_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '分类表主键',
  `category_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类的名称',
  `category_icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类的图标',
  `category_rank` int(0) NOT NULL DEFAULT 1 COMMENT '分类的排序值 被使用的越多数值越大',
  `is_deleted` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0=否 1=是',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_blog_category
-- ----------------------------
INSERT INTO `tb_blog_category` VALUES (20, 'About', '/admin/dist/img/category/10.png', 8, 1, '2018-11-12 00:28:49');
INSERT INTO `tb_blog_category` VALUES (22, 'SSM', '/admin/dist/img/category/00.png', 22, 0, '2018-11-12 10:42:25');
INSERT INTO `tb_blog_category` VALUES (24, '日常随笔', '/admin/dist/img/category/06.png', 36, 0, '2018-11-12 10:43:21');
INSERT INTO `tb_blog_category` VALUES (25, 'SpringBoot', '/admin/dist/img/category/06.png', 13, 0, '2020-11-04 09:45:49');
INSERT INTO `tb_blog_category` VALUES (26, 'MySQL', '/admin/dist/img/category/12.png', 2, 0, '2021-12-04 14:11:42');
INSERT INTO `tb_blog_category` VALUES (27, 'markdown', '/admin/dist/img/category/09.png', 2, 0, '2021-12-04 14:14:25');

-- ----------------------------
-- Table structure for tb_blog_comment
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_comment`;
CREATE TABLE `tb_blog_comment`  (
  `comment_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `blog_id` bigint(0) NOT NULL DEFAULT 0 COMMENT '关联的blog主键',
  `commentator` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '评论者名称',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '评论人的邮箱',
  `website_url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '网址',
  `comment_body` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '评论内容',
  `comment_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '评论提交时间',
  `commentator_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '评论时的ip地址',
  `reply_body` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '回复内容',
  `reply_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '回复时间',
  `comment_status` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否审核通过 0-未审核 1-审核通过',
  `is_deleted` tinyint(0) NULL DEFAULT 0 COMMENT '是否删除 0-未删除 1-已删除',
  PRIMARY KEY (`comment_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_blog_comment
-- ----------------------------
INSERT INTO `tb_blog_comment` VALUES (26, 4, '十三', '224683568@qq.com', '', '第一条评论', '2019-05-13 10:12:19', '', '', '2019-05-12 21:13:31', 1, 1);
INSERT INTO `tb_blog_comment` VALUES (27, 1, '学生', '16265959027@qq.com', '', 'ss', '2020-11-05 14:24:29', '', '', '2020-11-05 14:24:29', 1, 1);
INSERT INTO `tb_blog_comment` VALUES (28, 1, '刘海滨', '1258463805@qq.com', '', '测试123', '2020-11-05 16:59:09', '', '', '2020-11-05 16:59:09', 1, 1);

-- ----------------------------
-- Table structure for tb_blog_tag
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_tag`;
CREATE TABLE `tb_blog_tag`  (
  `tag_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '标签表主键id',
  `tag_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标签名称',
  `is_deleted` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0=否 1=是',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 143 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_blog_tag
-- ----------------------------
INSERT INTO `tb_blog_tag` VALUES (57, '世界上有一个很可爱的人', 0, '2018-11-12 00:31:15');
INSERT INTO `tb_blog_tag` VALUES (134, '配置文件', 0, '2020-11-03 17:17:37');
INSERT INTO `tb_blog_tag` VALUES (135, '学习笔记', 0, '2020-11-04 10:07:42');
INSERT INTO `tb_blog_tag` VALUES (138, 'MySQL', 0, '2021-12-04 14:12:43');
INSERT INTO `tb_blog_tag` VALUES (139, 'markdown', 0, '2021-12-04 14:15:48');
INSERT INTO `tb_blog_tag` VALUES (140, 'Spring', 0, '2021-12-04 14:23:07');
INSERT INTO `tb_blog_tag` VALUES (141, 'SpringMVC', 0, '2021-12-04 14:23:07');
INSERT INTO `tb_blog_tag` VALUES (142, 'MyBaits', 0, '2021-12-04 14:23:07');
INSERT INTO `tb_blog_tag` VALUES (143, 'SpringBoot', 0, '2021-12-04 14:23:57');

-- ----------------------------
-- Table structure for tb_blog_tag_relation
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_tag_relation`;
CREATE TABLE `tb_blog_tag_relation`  (
  `relation_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '关系表id',
  `blog_id` bigint(0) NOT NULL COMMENT '博客id',
  `tag_id` int(0) NOT NULL COMMENT '标签id',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`relation_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 324 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_blog_tag_relation
-- ----------------------------
INSERT INTO `tb_blog_tag_relation` VALUES (307, 7, 135, '2020-11-05 16:07:03');
INSERT INTO `tb_blog_tag_relation` VALUES (315, 9, 138, '2021-12-04 14:12:43');
INSERT INTO `tb_blog_tag_relation` VALUES (316, 10, 139, '2021-12-04 14:15:48');
INSERT INTO `tb_blog_tag_relation` VALUES (317, 1, 57, '2021-12-04 14:21:59');
INSERT INTO `tb_blog_tag_relation` VALUES (318, 5, 134, '2021-12-04 14:23:07');
INSERT INTO `tb_blog_tag_relation` VALUES (319, 5, 140, '2021-12-04 14:23:07');
INSERT INTO `tb_blog_tag_relation` VALUES (320, 5, 141, '2021-12-04 14:23:07');
INSERT INTO `tb_blog_tag_relation` VALUES (321, 5, 142, '2021-12-04 14:23:07');
INSERT INTO `tb_blog_tag_relation` VALUES (322, 6, 134, '2021-12-04 14:23:57');
INSERT INTO `tb_blog_tag_relation` VALUES (323, 6, 142, '2021-12-04 14:23:57');
INSERT INTO `tb_blog_tag_relation` VALUES (324, 6, 143, '2021-12-04 14:23:57');

-- ----------------------------
-- Table structure for tb_config
-- ----------------------------
DROP TABLE IF EXISTS `tb_config`;
CREATE TABLE `tb_config`  (
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '配置项的名称',
  `config_value` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '配置项的值',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`config_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_config
-- ----------------------------
INSERT INTO `tb_config` VALUES ('footerAbout', 'LIFE IS LIKE A PLAY', '2018-11-11 20:33:23', '2020-10-29 07:28:42');
INSERT INTO `tb_config` VALUES ('footerCopyRight', '@2020 LHB', '2018-11-11 20:33:31', '2020-10-29 07:28:42');
INSERT INTO `tb_config` VALUES ('footerICP', '赣ICP备xxxxxxxxxxx号-X', '2018-11-11 20:33:27', '2020-10-29 07:28:42');
INSERT INTO `tb_config` VALUES ('footerPoweredBy', 'https://gitee.com/liuhaibin789/projects', '2018-11-11 20:33:36', '2020-10-29 07:28:42');
INSERT INTO `tb_config` VALUES ('footerPoweredByURL', 'https://gitee.com/liuhaibin789/projects', '2018-11-11 20:33:39', '2020-10-29 07:28:42');
INSERT INTO `tb_config` VALUES ('websiteDescription', 'lhb~blog是SpringBoot2+Thymeleaf+Mybatis建造的个人博客网站', '2018-11-11 20:33:04', '2021-08-16 07:18:35');
INSERT INTO `tb_config` VALUES ('websiteIcon', '/blog/amaze/images/logo.png', '2018-11-11 20:33:11', '2021-08-16 07:18:35');
INSERT INTO `tb_config` VALUES ('websiteLogo', '/admin/dist/img/logo2.png', '2018-11-11 20:33:08', '2021-08-16 07:18:35');
INSERT INTO `tb_config` VALUES ('websiteName', 'lhb~blog', '2018-11-11 20:33:01', '2021-08-16 07:18:35');
INSERT INTO `tb_config` VALUES ('yourAvatar', '/admin/dist/img/13.png', '2018-11-11 20:33:14', '2020-10-29 07:25:01');
INSERT INTO `tb_config` VALUES ('yourEmail', '1626590287@qq.com', '2018-11-11 20:33:17', '2020-10-29 07:25:01');
INSERT INTO `tb_config` VALUES ('yourName', 'LHB', '2018-11-11 20:33:20', '2020-10-29 07:25:01');

-- ----------------------------
-- Table structure for tb_link
-- ----------------------------
DROP TABLE IF EXISTS `tb_link`;
CREATE TABLE `tb_link`  (
  `link_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '友链表主键id',
  `link_type` tinyint(0) NOT NULL DEFAULT 0 COMMENT '友链类别 0-友链 1-推荐 2-个人网站',
  `link_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '网站名称',
  `link_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '网站链接',
  `link_description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '网站描述',
  `link_rank` int(0) NOT NULL DEFAULT 0 COMMENT '用于列表排序',
  `is_deleted` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0-未删除 1-已删除',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`link_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_link
-- ----------------------------
INSERT INTO `tb_link` VALUES (20, 1, 'BiliBili弹幕网', 'https://www.bilibili.com/', 'B站大学', 0, 0, '2020-10-29 15:24:06');

-- ----------------------------
-- Table structure for tb_test
-- ----------------------------
DROP TABLE IF EXISTS `tb_test`;
CREATE TABLE `tb_test`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `test_info` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '测试内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_test
-- ----------------------------
INSERT INTO `tb_test` VALUES (1, 'SpringBoot-MyBatis测试');

SET FOREIGN_KEY_CHECKS = 1;
