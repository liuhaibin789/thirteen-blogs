package com.site.blog.my.core.config;

import org.springframework.util.ResourceUtils;

/**
 * 文件上传路径
 */
public class Constants {
    //上传文件的默认url前缀，根据部署设置自行修改
    //public final static String FILE_UPLOAD_DIC = "/opt/deploy/upload/";
    public final static String FILE_UPLOAD_DIC = "D:/opt/deploy/upload/";
}
