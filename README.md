# 十三博客

#### 介绍
感谢十三大佬的开源代码（https://github.com/ZHENFENG13），直接在理解代码的基础上修改了一下代码，这个仓库用了保存

#### 软件架构
使用 SpringBoot + Mybatis + Thymeleaf 

#### 使用说明

1下载代码，创建springboot项目，使用pom.xml下载依赖，把src下的项目复制粘贴进去，修改数据库路径和密码
2使用sql 进行数据库创建
#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
